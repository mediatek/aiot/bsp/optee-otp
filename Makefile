export V ?= 0

OUTPUT_DIR ?= $(CURDIR)/out

# If _HOST or _TA specific compilers are not specified, then use CROSS_COMPILE
HOST_CROSS_COMPILE ?= $(CROSS_COMPILE)
TA_CROSS_COMPILE ?= $(CROSS_COMPILE)

.PHONY: all
all:
	$(MAKE) -C host CROSS_COMPILE="$(HOST_CROSS_COMPILE)" --no-builtin-variables OUTPUT_DIR=$(OUTPUT_DIR)
	$(MAKE) -C ta CROSS_COMPILE="$(TA_CROSS_COMPILE)" LDFLAGS=""
	@mkdir -p $(OUTPUT_DIR)
	@mkdir -p $(OUTPUT_DIR)/ta
	cp ta/*.ta $(OUTPUT_DIR)/ta/

.PHONY: clean
clean:
	$(MAKE) -C host clean
	$(MAKE) -C ta clean
	rm -r -f $(OUTPUT_DIR)
