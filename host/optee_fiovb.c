/*
 * Copyright (c) 2019, Foundries.IO
 * Copyright (c) 2022, BayLibre
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */


#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <otp_ta.h>

#include "common.h"

#define	CMD_PRINTENV	"fiovb_printenv"
#define CMD_SETENV	"fiovb_setenv"
#define CMD_DELENV	"fiovb_delenv"
#define MAX_BUFFER	4096

static int fiovb_printenv(int argc, char *argv[])
{
	char buffer[MAX_BUFFER];
	struct test_ctx ctx;
	int res;

	if (argc != 2)
		return -1;

	prepare_tee_session(&ctx);

	res = read_secure_object(&ctx, argv[1], buffer, MAX_BUFFER);
	if (res == TEEC_SUCCESS)
		printf("%s\n", buffer);
	else
		fprintf(stderr, "Read persistent value for %s failed: %s\n",
			argv[1], strerror(-res));

	terminate_tee_session(&ctx);

	return res;
}

int fiovb_setenv(int argc, char *argv[])
{
	struct test_ctx ctx;
	int res;

	if (argc != 3)
		return -1;

	prepare_tee_session(&ctx);
	res = write_secure_object(
		&ctx, argv[1], argv[2], strlen(argv[2]) + 1);
	terminate_tee_session(&ctx);

	return res;
}

int fiovb_delenv(int argc, char *argv[])
{
        struct test_ctx ctx;
        int res;

	if (argc != 2)
		return -1;

        prepare_tee_session(&ctx);
        res = delete_secure_object(&ctx, argv[1]);
        terminate_tee_session(&ctx);

	return res;
}

int main(int argc, char *argv[])
{
	char *p, *cmdname = *argv;

	if ((p = strrchr (cmdname, '/')) != NULL)
		cmdname = p + 1;

	if (!strncmp(cmdname, CMD_PRINTENV, strlen(CMD_PRINTENV))) {
		if (fiovb_printenv (argc, argv)) {
			fprintf (stderr, "Cant print the environment\n");
			return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}

	if (!strncmp(cmdname, CMD_SETENV, strlen(CMD_SETENV))) {
		if (fiovb_setenv(argc, argv)) {
			fprintf (stderr, "Cant set the environment\n");
			return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}

	if (!strncmp(cmdname, CMD_DELENV, strlen(CMD_DELENV))) {
		if (fiovb_delenv(argc, argv)) {
			fprintf (stderr, "Cant delete the environment\n");
			return EXIT_FAILURE;
		}

		return EXIT_SUCCESS;
	}

	fprintf (stderr,
		"Identity crisis - may be called as `" CMD_PRINTENV
		"', as `" CMD_SETENV "' or as `" CMD_DELENV
		"', but not as `%s'\n", argv[0]);

	return EXIT_FAILURE;
}
