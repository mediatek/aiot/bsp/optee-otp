/*
 * Copyright (c) 2017, Linaro Limited
 * Copyright (c) 2022, BayLibre
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <err.h>
#include <errno.h>
#include <stdio.h>
#include <string.h>

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include <otp_ta.h>

#include "common.h"

int read_mac(void)
{
	struct test_ctx ctx;
	char mac_data[MAC_ADDRESS_LEN];
	int res;

	prepare_tee_session(&ctx);

	res = read_secure_object(&ctx, "mac", mac_data, sizeof(mac_data));
	if (res == TEEC_SUCCESS) {
		printf("%02hhx:%02hhx:%02hhx:%02hhx:%02hhx:%02hhx\n",
			mac_data[0], mac_data[1], mac_data[2], 
			mac_data[3], mac_data[4], mac_data[5]);
	}

	terminate_tee_session(&ctx);

	return res;
}

int read_serial(void)
{
	struct test_ctx ctx;
	char serial_data[SN_LEN];
	int res;

	prepare_tee_session(&ctx);

	memset(serial_data, 0, SN_LEN);
	res = read_secure_object(&ctx, "serial",
				serial_data, sizeof(serial_data));
	if (res == TEEC_SUCCESS)
		printf("%s\n", serial_data);

	terminate_tee_session(&ctx);

	return res;
}

int read_demo(void)
{
	struct test_ctx ctx;
	char demo_data[SN_LEN];
	int res;

	prepare_tee_session(&ctx);

	memset(demo_data, 0, SN_LEN);
	res = read_secure_object(&ctx, "demo",
				demo_data, sizeof(demo_data));
	if (res == TEEC_SUCCESS)
		printf("%s\n", demo_data);

	terminate_tee_session(&ctx);

	return res;
}

void print_help(void) {
	printf("optee_otp get mac\noptee_otp get sn\n"
		"optee_otp set mac <MAC>\noptee_otp set sn <SN>\n");
	exit(EINVAL);
}

int main(int argc, char **argv)
{
	char mac_data[MAC_ADDRESS_LEN];
	struct test_ctx ctx;
	int set = 0;
	int mac = 0;
	int sn = 0;
	int demo = 0;
	int res;

	if (argc < 3 || (argc == 3 && !strcmp(argv[1], "set")))
		print_help();

	if (!strcmp(argv[1], "set"))
		set = 1;
	else if (!strcmp(argv[1], "get"))
		set = 0;
	else
		print_help();

	if (!strcmp(argv[2], "sn"))
		sn = 1;

	else if (!strcmp(argv[2], "mac"))
		mac = 1;
	else if (!strcmp(argv[2], "demo"))
		demo = 1;
	else
		print_help();

	if (!set) {
		if (sn)
			read_serial();
		if (mac)
			read_mac();
		if (demo)
			read_demo();
	} else {
		if (sn) {
			if (strlen(argv[3]) >= SN_LEN)
				errx(-EINVAL, "Serial number too long\n");

			prepare_tee_session(&ctx);
			res = write_secure_object(
				&ctx, "serial", argv[3], strlen(argv[3]) + 1);
			terminate_tee_session(&ctx);
		}
		if (mac) {
			res = sscanf(argv[3], "%2hhx:%2hhx:%2hhx:%2hhx:%2hhx:%2hhx\n",
					&mac_data[0], &mac_data[1], &mac_data[2],
					&mac_data[3], &mac_data[4], &mac_data[5]);
			if (res != 6)
				errx(-EINVAL, "Invalid mac address\n");

			prepare_tee_session(&ctx);
			res = write_secure_object(
				&ctx, "mac", mac_data, sizeof(mac_data));
			terminate_tee_session(&ctx);
		}
		if (demo) {
			if (strlen(argv[3]) >= SN_LEN)
				errx(-EINVAL, "Demo too long\n");

			prepare_tee_session(&ctx);
			res = write_rw_secure_object(
				&ctx, "demo", argv[3], strlen(argv[3]) + 1);
			terminate_tee_session(&ctx);
		}
	}

	return 0;
}
