// SPDX-License-Identifier: BSD-2-Clause
/* Copyright (c) 2018, Linaro Limited */
/* Copyright (c) 2022, BayLibre */

#include <inttypes.h>
#include <otp_ta.h>
#include <tee_internal_api.h>
#include <tee_internal_api_extensions.h>

#include <string.h>

static const uint32_t storageid = TEE_STORAGE_PRIVATE_RPMB;

static TEE_Result write_persist_value(uint32_t pt,
				      TEE_Param params[TEE_NUM_PARAMS],
				      uint32_t flags)
{
	const uint32_t exp_pt = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
						TEE_PARAM_TYPE_MEMREF_INPUT,
						TEE_PARAM_TYPE_NONE,
						TEE_PARAM_TYPE_NONE);
	TEE_Result res;
	TEE_ObjectHandle h;
	char *name;
	char *value;
	uint32_t name_sz;
	uint32_t value_sz;

	flags |= TEE_DATA_FLAG_ACCESS_READ |
		 TEE_DATA_FLAG_ACCESS_WRITE;

	if (pt != exp_pt)
		return TEE_ERROR_BAD_PARAMETERS;

	name_sz = params[0].memref.size;
	value_sz = params[1].memref.size;
	if (!name_sz || !value_sz)
		return TEE_ERROR_BAD_PARAMETERS;

	name = TEE_Malloc(name_sz, 0);
	if (!name)
		return TEE_ERROR_OUT_OF_MEMORY;

	value = TEE_Malloc(value_sz, 0);
	if (!value) {
		TEE_Free(name);
		return TEE_ERROR_OUT_OF_MEMORY;
	}

	TEE_MemMove(name, params[0].memref.buffer, name_sz);
	TEE_MemMove(value, params[1].memref.buffer, value_sz);

	res = TEE_CreatePersistentObject(storageid, name, name_sz,
					 flags, NULL, value, value_sz, &h);
	if (res)
		EMSG("Can't create named object value, res = 0x%x", res);

	TEE_CloseObject(h);
	TEE_Free(value);
	TEE_Free(name);

	return res;
}

static TEE_Result read_persist_value(uint32_t pt,
				      TEE_Param params[TEE_NUM_PARAMS])
{
	const uint32_t exp_pt = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
						TEE_PARAM_TYPE_MEMREF_OUTPUT,
						TEE_PARAM_TYPE_NONE,
						TEE_PARAM_TYPE_NONE);
	uint32_t flags = TEE_DATA_FLAG_ACCESS_READ;
	TEE_Result res;
	TEE_ObjectHandle h;
	char *name;
	char *value;
	uint32_t name_sz;
	uint32_t value_sz;
	uint32_t count;

	if (pt != exp_pt)
		return TEE_ERROR_BAD_PARAMETERS;

	name_sz = params[0].memref.size;
	value_sz = params[1].memref.size;
	if (!name_sz || !value_sz)
		return TEE_ERROR_BAD_PARAMETERS;

	name = TEE_Malloc(name_sz, 0);
	if (!name)
		return TEE_ERROR_OUT_OF_MEMORY;

	value = TEE_Malloc(value_sz, 0);
	if (!value) {
		TEE_Free(name);
		return TEE_ERROR_OUT_OF_MEMORY;
	}

	TEE_MemMove(name, params[0].memref.buffer, name_sz);

	res = TEE_OpenPersistentObject(storageid, name, name_sz, flags, &h);
	if (res) {
		EMSG("Can't open named object value, res = 0x%x", res);
		goto out_free;
	}

	res =  TEE_ReadObjectData(h, value, value_sz, &count);
	if (res) {
		EMSG("Can't read named object value, res = 0x%x", res);
		goto out;
	}

	TEE_MemMove(params[1].memref.buffer, value,
		    value_sz);

	params[1].memref.size = count;

out:
	TEE_CloseObject(h);
out_free:
	TEE_Free(value);
	TEE_Free(name);

	return res;
}

static TEE_Result delete_persist_value(uint32_t pt,
				       TEE_Param params[TEE_NUM_PARAMS])
{
	const uint32_t exp_pt = TEE_PARAM_TYPES(TEE_PARAM_TYPE_MEMREF_INPUT,
						TEE_PARAM_TYPE_NONE,
						TEE_PARAM_TYPE_NONE,
						TEE_PARAM_TYPE_NONE);
	const uint32_t flags = TEE_DATA_FLAG_ACCESS_READ |
			       TEE_DATA_FLAG_ACCESS_WRITE_META;
	TEE_Result res;
	TEE_ObjectHandle h;
	char *name;
	size_t name_sz;

	if (pt != exp_pt)
		return TEE_ERROR_BAD_PARAMETERS;

	name_sz = params[0].memref.size;
	if (!name_sz)
		return TEE_ERROR_BAD_PARAMETERS;

	name = TEE_Malloc(name_sz, 0);
	if (!name)
		return TEE_ERROR_OUT_OF_MEMORY;

	TEE_MemMove(name, params[0].memref.buffer, name_sz);

	res = TEE_OpenPersistentObject(storageid, name,
				       name_sz, flags, &h);
	if (res) {
		EMSG("Failed to open persistent object, res = 0x%x", res);
		return res;
	}

	res = TEE_CloseAndDeletePersistentObject1(h);
	if (res)
		EMSG("Failed to delete persistent object, res = 0x%x", res);

	return res;
}

TEE_Result TA_CreateEntryPoint(void)
{
	/* Nothing to do */
	return TEE_SUCCESS;
}

void TA_DestroyEntryPoint(void)
{
	/* Nothing to do */
}

TEE_Result TA_OpenSessionEntryPoint(uint32_t __unused param_types,
				    TEE_Param __unused params[4],
				    void __unused **session)
{
	/* Nothing to do */
	return TEE_SUCCESS;
}

void TA_CloseSessionEntryPoint(void __unused *session)
{
	/* Nothing to do */
}

TEE_Result TA_InvokeCommandEntryPoint(void __unused *session,
				      uint32_t command,
				      uint32_t param_types,
				      TEE_Param params[4])
{
	switch (command) {
	case TA_OTP_CMD_WRITE_RAW:
		return write_persist_value(param_types, params, 0);
	case TA_OTP_CMD_WRITE_RW_RAW:
		return write_persist_value(param_types, params,
					   TEE_DATA_FLAG_OVERWRITE);
	case TA_OTP_CMD_READ_RAW:
		return read_persist_value(param_types, params);
	case TA_OTP_CMD_DELETE:
		return delete_persist_value(param_types, params);
	default:
		EMSG("Command ID 0x%x is not supported", command);
		return TEE_ERROR_NOT_SUPPORTED;
	}
}
